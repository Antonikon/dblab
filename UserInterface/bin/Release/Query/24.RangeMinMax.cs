public static StatusError Run(CoreModule core, CoreModule.BaseTable target) {

    CoreModule.BaseTable argTable;
    var status = core.GetTable("Sequence_Args", out argTable);
    if (status != StatusError.None) return status;

    List<EntityInfo> resultColumns = new List<EntityInfo>();
    resultColumns.Add(new EntityInfo("Time", EntityType.Int));
    resultColumns.Add(new EntityInfo("Type", EntityType.String));
    resultColumns.Add(new EntityInfo("Value", EntityType.Float));

    foreach (var argLine in argTable) {
        Console.WriteLine(argLine.Count);
        long begin = argLine[1].ToInt();
        long end = argLine[2].ToInt();

        CoreModule.BaseTable resultTable;
        status = core.CreateTable(core.GenerateTableName(string.Format("{0}_RangeMinMax_Result[{1}-{2}]_", target.GetName(), begin, end)), resultColumns, 0, out resultTable);
        if (status != StatusError.None) return status;

        long minTime = 0;
        long maxTime = 0;
        double min = double.MaxValue;
        double max = double.MinValue;

        foreach (var line in target) {
            long time = line[0].ToInt();
            if (time < begin) continue;
            if (time > end) break;

            double value = line[1].ToFloat();
            if (value < min) {
                min = value;
                minTime = time;
            }
            if (value > max) {
                max = value;
                maxTime = time;
            }
        }

        resultTable.InsertLine(maxTime, "MAX", max);
        resultTable.InsertLine(minTime, "MIN", min);
    }

    return StatusError.None;
}