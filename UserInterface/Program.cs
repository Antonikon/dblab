﻿using System;
using System.Collections.Generic;
using Gtk;
using UserInterface;
using System.Text.RegularExpressions;

namespace DBLab {
    class Program {
        public static CoreModule Core = new CoreModule(new FileTable.FileModule("Tables"));
        public static MainWindow WindowMain;

        public static bool ProcessStatusError(Window parent, StatusError error, string additionInfo = "") {
            if (error == StatusError.None) return true;

            string msg;
            switch (error) {
                case StatusError.TableAlreadyExist:
                    msg = "Table already exist";
                    break;
                case StatusError.HeaderParseError:
                    msg = "Header parse error";
                    break;
                case StatusError.LineParseError:
                    msg = "Table content parse error";
                    break;
                case StatusError.TableNotFound:
                    msg = "Table not found";
                    break;
                case StatusError.InvalidTableName:
                    msg = "Invalid table name";
                    break;
                case StatusError.PrimaryKeyNotFound:
                    msg = "Table doesnt contain primary key";
                    break;
                case StatusError.ValueParseError:
                    msg = "Invalid entity value format";
                    break;
                case StatusError.FieldNameIsEmpty:
                    msg = "Field name cannot be empty";
                    break;
                case StatusError.FieldRedeclaration:
                    msg = "Field redeclaration error";
                    break;
                case StatusError.FieldDoesntExist:
                    msg = "Field doesnt exist";
                    break;
                case StatusError.EmptyTableName:
                    msg = "Table's name is empty";
                    break;
                case StatusError.KeyIsEmpty:
                    msg = "Key value is emty";
                    break;
                case StatusError.KeyNotFound:
                    msg = "Key not found";
                    break;
                case StatusError.KeyAlreadyExist:
                    msg = "Key already exist";
                    break;
                case StatusError.KeyCannotBeChanged:
                    msg = "Key cannot be changed";
                    break;
                case StatusError.QueryCompileError:
                    msg = "Query compilation error";
                    break;
                case StatusError.QueryRuntimeError:
                    msg = "Query runtime error";
                    break;
                default:
                    msg = "";
                    break;
            }
            if (additionInfo.Length > 0) msg = msg + "\n\n" + additionInfo;

            var dialog = new Gtk.MessageDialog(parent, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.Ok, false, "{0}", msg);
            dialog.Title = "Error";


            if (dialog.Run() == (int)Gtk.ResponseType.Ok) dialog.Destroy();

            return false;
        }


        public static void Main(string[] args) {
            Application.Init();
            WindowMain = new MainWindow();
            WindowMain.Show();
            Application.Run();
        }
    }
}
