﻿using System;
using System.Collections.Generic;
using DBLab;

//меню сздания таблицы
namespace UserInterface {
    public partial class CreateTableMenu : Gtk.Window {
        public CreateTableMenu() :
                base(Gtk.WindowType.Toplevel)
        {
            Build();

            var nameColumn = new Gtk.TreeViewColumn();
            nameColumn.Title = "Name";

            var typeColumn = new Gtk.TreeViewColumn();
            typeColumn.Title = "Type";


            ValueList.AppendColumn(nameColumn);
            ValueList.AppendColumn(typeColumn);

            ValueList.Model = new Gtk.ListStore(typeof(string), typeof(string));

            var nameCell = new Gtk.CellRendererText();
            var typeCell = new Gtk.CellRendererText();
            ValueList.Columns[0].PackStart(nameCell, true);
            ValueList.Columns[1].PackStart(typeCell, true);

            ValueList.Columns[0].AddAttribute(nameCell, "text", 0);
            ValueList.Columns[1].AddAttribute(typeCell, "text", 1);
        }

        protected void OnAddActionActivated(object sender, EventArgs e) {
            if (ValueName.Text.Length == 0) return;

            Gtk.ListStore list = (Gtk.ListStore)ValueList.Model;
            list.AppendValues(ValueName.Text, ValueType.ActiveText);
            ValueName.Text = "";

            list = (Gtk.ListStore)KeyValue.Model;
            list.Clear();

            Gtk.TreeIter it;
            ValueList.Model.GetIterFirst(out it);
            do {
                list.AppendValues((string)ValueList.Model.GetValue(it, 0));
            } while (ValueList.Model.IterNext(ref it));

            KeyValue.Model.GetIterFirst(out it);
            KeyValue.SetActiveIter(it);
        }

        protected void OnRemoveActionActivated(object sender, EventArgs e) {
            Gtk.TreeIter it;
            ValueList.Selection.GetSelected(out it);
            var list = (Gtk.ListStore)ValueList.Model;
            list.Remove(ref it);
        }

        protected void OnCreateButtonClicked(object sender, EventArgs e) {
            List<EntityInfo> valueInfo = new List<EntityInfo>();
            int keyIndex = 0;


            Gtk.TreeIter it;
            int index = 0;
            ValueList.Model.GetIterFirst(out it);
            do {
                string name = (string)ValueList.Model.GetValue(it, 0);
                string typeName = (string)ValueList.Model.GetValue(it, 1);
                EntityType type = typeName.ToEntityType();

                valueInfo.Add(new EntityInfo(name, type));

                if (name == KeyValue.ActiveText) keyIndex = index;
                index++;
            } while (ValueList.Model.IterNext(ref it));

            var status = Program.Core.CreateTable(TableName.Text, valueInfo, keyIndex);
            if (Program.ProcessStatusError(this, status)) {
                Program.WindowMain.UpdateTableList();
                Destroy();
            }
        }

        protected void OnCancelButtonClicked(object sender, EventArgs e) {
            Destroy();
        }
    }
}
