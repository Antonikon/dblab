﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using Gtk;
using DBLab;
using System.Threading;

//главное меню
public partial class MainWindow : Gtk.Window {
    private Thread QueryThread = null;

    public MainWindow() : base(Gtk.WindowType.Toplevel) {
        Build();

        var column = new Gtk.TreeViewColumn();
        column.Title = "Table";

        TableList.AppendColumn(column);

        TableList.Model = new Gtk.ListStore(typeof(string));

        var tableCell = new Gtk.CellRendererText();
        tableCell.Editable = true;
        tableCell.Edited += OnTableRenamed;

        TableList.Columns[0].PackStart(tableCell, true);
        TableList.Columns[0].AddAttribute(tableCell, "text", 0);

        UpdateTableList();
        RefreshQueryList();
    }

    private string GetCurrentTable() {
        TreeIter it;
        if (TableList.Selection.GetSelected(out it)) {
            var list = (Gtk.ListStore)TableList.Model;
            return (string)list.GetValue(it, 0);
        } else return "";
    }

    private void ClearTableContent() {
        if (TableContent.Model != null) {
            var list = (Gtk.ListStore)TableContent.Model;
            list.Clear();
        }
        if (TableContent.Columns.Length > 0) {
            foreach (var c in TableContent.Columns) {
                TableContent.RemoveColumn(c);
            }
        }
    }

    private StatusError GetSelectedKey(out EntityValue value) {
        value = null;

        TreeIter it;
        TableContent.Selection.GetSelected(out it);
        var list = (Gtk.ListStore)TableContent.Model;

        CoreModule.BaseTable table;
        var status = Program.Core.GetTable(GetCurrentTable(), out table);
        if (status != StatusError.None) return status;

        string rawKey = (string)list.GetValue(it, (int)table.GetKeyIndex());

        value = new EntityValue(table.GetKeyInfo().Type, rawKey);

        return StatusError.None;
    }

    public void UpdateTableContent() {
        ClearTableContent();

        TreeIter it;
        if (TableList.Selection.GetSelected(out it)) {
            var list = (Gtk.ListStore)TableList.Model;

            string name = (string)list.GetValue(it, 0);
            CoreModule.BaseTable table;
            var status = Program.Core.GetTable(name, out table);

            if (Program.ProcessStatusError(this, status)) {
                var contentList = new Gtk.ListStore(Enumerable.Repeat<Type>(typeof(string), (int)table.GetCoulumnCount()).ToArray());
                TableContent.Model = contentList;
                for (int i = 0; i < table.GetCoulumnCount(); i++) {
                    var column = new Gtk.TreeViewColumn();
                    column.Title = table.GetEntityInfo(i).Name;

                    var render = new Gtk.CellRendererText();
                    render.Editable = true;
                    render.Edited += OnValueEdited;
                    if (i == table.GetKeyIndex()) render.Background = "#d2d2d2";

                    TableContent.AppendColumn(column);
                    column.PackStart(render, true);
                    column.AddAttribute(render, "text", i);
                    //column.SetAttributes(render, "background", "#FF0000");
                    //TableContent.AppendColumn(table.GetEntityInfo(i).Name, render, "text", i);

                }

                foreach (var line in table) {
                    string[] values = new string[line.Count];
                    for (int i = 0; i < line.Count; i++) {
                        values[i] = line[i].ToString();
                    }
                    contentList.AppendValues(values);
                }
            }
        }
    }

    private void RefreshQueryList() {
        var files = System.IO.Directory.GetFiles("Query");

        var list = (Gtk.ListStore)QueryList.Model;
        list.Clear();

        foreach (var name in files) {
            list.AppendValues(System.IO.Path.GetFileName(name));
        }
    }

    private void InsertLine() {
        string rawKey = KeyText.Text;
        CoreModule.BaseTable table;
        var status = Program.Core.GetTable(GetCurrentTable(), out table);
        if (Program.ProcessStatusError(this, status)) {
            EntityValue key;
            if (EntityValue.SafeCreate(table.GetKeyInfo().Type, rawKey, out key)) {

                List<EntityValue> line = new List<EntityValue>((int)table.GetCoulumnCount());
                for (int i = 0; i < table.GetCoulumnCount(); i++) {
                    if (i == table.GetKeyIndex()) line.Add(key);
                    else line.Add(new EntityValue(table.GetEntityInfo(i).Type));
                }
                status = table.InsertLine(line);
                if (Program.ProcessStatusError(this, status)) UpdateTableContent();
            } else Program.ProcessStatusError(this, StatusError.ValueParseError);
            KeyText.Text = "";
        }
    }

    private StatusError LaunchQueryThread(Gtk.MessageDialog dialog, string code, out string error, string targetTableName = "") {
        var status = Program.Core.ExecuteQuery(code, out error, targetTableName);
        dialog.Respond((int)Gtk.ResponseType.Close);
        return status;
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a) {
        Program.Core.Close();
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnNewButtonClicked(object sender, EventArgs e) {
        var menu = new UserInterface.CreateTableMenu();
        menu.Show();
    }


    protected void OnDeleteButtonClicked(object sender, EventArgs e) {
        Program.Core.RemoveTable(GetCurrentTable());
        ClearTableContent();
        UpdateTableList();
    }

    protected void OnCloseButtonClicked(object sender, EventArgs e) {
        Program.Core.CloseTable(GetCurrentTable());
        ClearTableContent();
        UpdateTableList();
    }

    protected void OnExecButtonClicked(object sender, EventArgs e) {
        var file = File.ReadAllText("Query/" + QueryList.ActiveText);
        var dialog = new Gtk.MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Info, ButtonsType.None, "Executing query...");
        dialog.Title = "Query Status";
        dialog.AddButton("Abort", (int)Gtk.ResponseType.Cancel);
        //LaunchQueryThread(dialog, file, GetCurrentTable());
        string error = "";
        StatusError status = StatusError.None;
        QueryThread = new Thread(() => status = LaunchQueryThread(dialog, file, out error, GetCurrentTable()));
        QueryThread.Start();

        if (dialog.Run() == (int)Gtk.ResponseType.Cancel) {
            QueryThread.Abort();
            dialog.Destroy();
        } else {
            QueryThread.Join();
            dialog.Destroy();
            Program.ProcessStatusError(this, status, error);
        }

        UpdateTableList();
        UpdateTableContent();
        /*string error;
        var status = Program.Core.ExecuteQuery(file, out error, GetCurrentTable());
        if (Program.ProcessStatusError(this, status, error)) {
            UpdateTableList();
            UpdateTableContent();
        }*/
    }

    public void UpdateTableList() {
        string lastSelection = GetCurrentTable();
        int index = 0;
        int selectIndex = -1;

        var list = (Gtk.ListStore)TableList.Model;
        list.Clear();
        foreach (var name in DBLab.Program.Core.GetTableList().Keys) {
            list.AppendValues(name);
            if (name == lastSelection) {
                selectIndex = index;
            }
            index++;
        }

        if (selectIndex != -1) {
            TreeIter it;
            list.IterNthChild(out it, selectIndex);
            TableList.Selection.SelectIter(it);
        }
    }

    protected void OnTableListCursorChanged(object sender, EventArgs e) {
        UpdateTableContent();
    }

    protected void OnAddActionActivated(object sender, EventArgs e) {
        InsertLine();
    }

    protected void OnRemoveActionActivated(object sender, EventArgs e) {
        CoreModule.BaseTable table;
        var status = Program.Core.GetTable(GetCurrentTable(), out table);
        if (Program.ProcessStatusError(this, status)) {
            EntityValue key;
            status = GetSelectedKey(out key);

            if (Program.ProcessStatusError(this, status)) {
                table.RemoveLine(key);
                var list = (Gtk.ListStore)TableContent.Model;
                TreeIter it;
                TableContent.Selection.GetSelected(out it);
                list.Remove(ref it);
            }
        }
    }

    protected void OnRefreshButtonClicked(object sender, EventArgs e) {
        RefreshQueryList();
    }

    protected void OnTableRefreshButtonClicked(object sender, EventArgs e) {
        UpdateTableList();
    }

    private void OnTableRenamed(object sender, EditedArgs args) {
        string name = GetCurrentTable();
        if (name == args.NewText) return;
        var status = Program.Core.RenameTable(name, args.NewText);
        if (Program.ProcessStatusError(this, status)) {
            UpdateTableList();
            UpdateTableContent();
        }
    }

    private void OnValueEdited(object sender, EditedArgs args) {

        CoreModule.BaseTable table;
        var status = Program.Core.GetTable(GetCurrentTable(), out table);
        if (Program.ProcessStatusError(this, status)) {
            var list = (Gtk.ListStore)TableContent.Model;

            var path = new Gtk.TreePath(args.Path);
            TreeIter it;
            TableContent.Model.GetIter(out it, path);

            int column = 0;
            for (int i = 0; i < TableContent.Columns.Length; i++) {
                if (TableContent.Columns[i].CellRenderers[0] == sender) {
                    column = i;
                    break;
                }
            }

            if (list.GetValue(it, column).ToString() == args.NewText) return;

            var key = new EntityValue(table.GetKeyInfo().Type, list.GetValue(it, table.GetKeyIndex()).ToString());

            EntityValue value;
            if (EntityValue.SafeCreate(table.GetEntityInfo(column).Type, args.NewText, out value)) {
                if (column == table.GetKeyIndex()) {
                    var dialog = new Gtk.MessageDialog(this, DialogFlags.DestroyWithParent, MessageType.Question, ButtonsType.YesNo, "Do you want to duplicate line?");
                    dialog.Title = "Key Editing";
                    if (dialog.Run() == (int)Gtk.ResponseType.Yes) {
                        var line = new List<EntityValue>(table.GetLine(key));
                        line[table.GetKeyIndex()] = value;
                        status = table.InsertLine(line);
                    } else {
                        status = table.UpdateKey(key, value);
                    }
                    dialog.Destroy();

                } else {
                    status = table.SetValue(key, column, value);
                }
                //var line = new List<EntityValue>(Target.GetLine(Key));
                //line[Target.GetKeyIndex()] = value;
                //var status = Target.InsertLine(line);

                if (Program.ProcessStatusError(this, status)) {
                    UpdateTableContent();
                }
            } else Program.ProcessStatusError(this, StatusError.ValueParseError);
        }
    }

    protected void OnEditActionActivated(object sender, EventArgs e) {
        TreeIter it;
        if (TableList.Selection.GetSelected(out it)) {
            var list = (Gtk.ListStore)TableList.Model;
            TableList.SetCursor(list.GetPath(it), TableList.Columns[0], true);
        }
    }

    protected void OnKeyTextActivated(object sender, EventArgs e) {
        InsertLine();
    }
}
