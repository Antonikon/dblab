﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.CodeDom.Compiler;
using System.Text;
using Microsoft.CSharp;
using System.IO;

namespace DBLab {
    
    //Типы переменой для таблицы
    public enum EntityType { 
        None,
        Int,
        String,
        Float
    }

    //Типы ошибок
    public enum StatusError {
        None = 0,
        TableAlreadyExist,
        TableNotFound,
        InvalidTableName,
        HeaderParseError,
        LineParseError,
        ValueParseError,
        PrimaryKeyNotFound,
        FieldNameIsEmpty,
        FieldRedeclaration,
        FieldDoesntExist,
        EmptyTableName,
        KeyIsEmpty,
        KeyNotFound,
        KeyAlreadyExist,
        KeyCannotBeChanged,
        QueryCompileError,
        QueryRuntimeError
    }

    //Информация о типе переменной(столбца) и её имя
    public class EntityInfo {
        public readonly string Name = "";
        public readonly EntityType Type = EntityType.Int;

        public EntityInfo(string name, EntityType type) {
            Name = name;
            Type = type;
        }

        public EntityInfo(EntityInfo other) {
            Name = other.Name;
            Type = other.Type;
        }

        public string GetTypeName() {
            switch (Type) {
                case EntityType.Int: return "int";
                case EntityType.Float: return "float";
                case EntityType.String: return "string";
                default: return "";
            }
        }
    }

    //Класс-значение ячейки таблицы
    public class EntityValue: IComparable {
        public readonly EntityType Type;
        private readonly object Value;

        public EntityValue(long value) {
            Type = EntityType.Int;
            Value = (object)value;
        }

        public EntityValue(double value) {
            Type = EntityType.Float;
            Value = (object)value;
        }

        public EntityValue(string value) {
            Type = EntityType.String;
            Value = (object)value;
        }

        public EntityValue(EntityType type) {
            Type = type;
            switch (Type) {
                default:
                case EntityType.Int:
                    Value = (int)0;
                    break;
                case EntityType.Float:
                    Value = 0.0;
                    break;
                case EntityType.String:
                    Value = "";
                    break;
            }
        }

        public EntityValue(EntityType type, string data) {
            Type = type;
            switch (Type) {
                case EntityType.Int:
                    Value = Convert.ToInt64(data);
                    break;
                case EntityType.Float:
                    Value = Convert.ToDouble(data);
                    break;
                case EntityType.String:
                    Value = data;
                    break;
            }
        }

        public static implicit operator EntityValue(string value) {
            return new EntityValue(value);
        }

        public static implicit operator EntityValue(long value) {
            return new EntityValue(value);
        }

        public static implicit operator EntityValue(double value) {
            return new EntityValue(value);
        }

        public static bool operator ==(EntityValue a, EntityValue b) {
            return a.Value.Equals(b.Value);
        }

        public static bool operator !=(EntityValue a, EntityValue b) {
            return !a.Value.Equals(b.Value);
        }

        public static bool operator <(EntityValue a, EntityValue b) {
            if (a.Type != b.Type) throw new ArgumentException();
            switch (a.Type) {
                case EntityType.Int: return (long)a.Value < (long)b.Value;
                case EntityType.Float: return (double)a.Value < (double)b.Value;
                case EntityType.String: return ((string)a.Value).CompareTo((string)b.Value) < 0;
                default: throw new ArgumentException();
            }
        }

        public static bool operator >(EntityValue a, EntityValue b) {
            if (a.Type != b.Type) throw new ArgumentException();
            switch (a.Type) {
                case EntityType.Int: return (long)a.Value > (long)b.Value;
                case EntityType.Float: return (double)a.Value > (double)b.Value;
                case EntityType.String: return ((string)a.Value).CompareTo((string)b.Value) > 0;
                default: throw new ArgumentException();
            }
        }

        public static bool SafeCreate(EntityType type, string data, out EntityValue value) {
            value = null;
            switch (type) {
                case EntityType.Int: {
                    long result;
                    if (!long.TryParse(data, out result)) return false;
                    value = new EntityValue(result);
                    return true;
                } case EntityType.Float: {
                    double result;
                    if (!double.TryParse(data, out result)) return false;
                    value = new EntityValue(result);
                    return true;
                } case EntityType.String: {
                    value = new EntityValue(data);
                    return true;
                } default: return false;

            }
        }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType()) return false;
            else return Value.Equals(((EntityValue)obj).Value);
        }

        public override int GetHashCode() {
            return Value.GetHashCode();
        }

        public long ToInt() { return (long)Value; }
        public double ToFloat() { return (double)Value; }

        public override string ToString() { return Value.ToString(); }

        public int CompareTo(object obj) {
            if (GetType() != obj.GetType()) throw new ArgumentException();
            EntityValue other = (EntityValue)obj;
            if (this < other) return -1;
            else if (this > other) return 1;
            else return 0;
        }
    }

    public static class EntityExtension {
        public static EntityType ToEntityType(this string str) {
            str = str.ToLower();
            switch (str) {
                case "int": return EntityType.Int;
                case "float": return EntityType.Float;
                case "string": return EntityType.String;
                default: return EntityType.None;
            }
        }
    }

    //Интерфейс для слоя DataLayer
    public interface DataModule {
        StatusError Open(string name, out CoreModule.BaseTable table);
        StatusError Create(string name, List<EntityInfo> valueInfo, int keyItem, out CoreModule.BaseTable table);
        bool Close(CoreModule.BaseTable table);
        bool Remove(string name);
        StatusError Rename(string name, string newName);
        string[] GetTableList();
    }

    //Ядро БД, остальные модули основываются на ядре
    public class CoreModule {

        //"Интерфейс" таблицы
        public abstract class BaseTable: IEnumerable <List<EntityValue>> {
            protected string Name;
            protected List<EntityInfo> ValueInfo = new List<EntityInfo>();
            protected int KeyIndex = -1;

            protected BaseTable() { }

            public IList<EntityInfo> GetEntitiesInfo() { return ValueInfo.AsReadOnly(); }
            public EntityInfo GetEntityInfo(string name) {
                foreach (var i in ValueInfo) {
                    if (i.Name == name) return i;
                }
                throw new ArgumentException();
            }

            public EntityInfo GetEntityInfo(int index) {
                return ValueInfo[index];
            }


            public int GetKeyIndex() { return KeyIndex; }
            public EntityInfo GetKeyInfo() { return ValueInfo[(int)KeyIndex]; }
            public string GetName() { return Name; }
            public int GetCoulumnCount() { return ValueInfo.Count; }

            public abstract IList<EntityValue> GetLine(EntityValue key);
            public abstract EntityValue GetValue(EntityValue key, string name);
            public abstract EntityValue GetValue(EntityValue key, int index);
            public abstract StatusError SetValue(EntityValue key, string name, EntityValue value);
            public abstract StatusError SetValue(EntityValue key, int index, EntityValue value);
            public abstract StatusError UpdateLine(List<EntityValue> line);
            public abstract StatusError InsertLine(List<EntityValue> line);
            public StatusError InsertLine(params EntityValue[] line) { return InsertLine(new List<EntityValue>(line)); }
            public abstract StatusError RemoveLine(EntityValue key);
            public abstract bool Contains(EntityValue key);
            public abstract StatusError UpdateKey(EntityValue key, EntityValue newKey);
            public abstract int GetLineCount();

            public int GetColumnIndex(string name) {
                for (int i = 0; i < ValueInfo.Count; i++) {
                    if (ValueInfo[i].Name == name) return i;
                }
                return -1;
            }

            public abstract IEnumerator<List<EntityValue>> GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() {
                return (IEnumerator)GetEnumerator();
            }
        }

        private Dictionary<string, BaseTable> TableList = new Dictionary<string, BaseTable>();
        private DataModule DataController;

        public CoreModule(DataModule dataController) {
            DataController = dataController;
            string[] list = DataController.GetTableList();
            foreach (var name in list) {
                TableList[name] = null;
            }
        }

        public void Close() {
            foreach (BaseTable table in TableList.Values) {
                if (table != null) DataController.Close(table);
            }
        }

        public StatusError CreateTable(string name, List<EntityInfo> valueInfo, int keyItem, out BaseTable table) {
            table = null;
            if (name.Length == 0) return StatusError.EmptyTableName;
            if (TableList.ContainsKey(name)) return StatusError.TableAlreadyExist;
            var status = ValidateValueInfo(valueInfo);
            if (status != StatusError.None) return status;
            status = DataController.Create(name, valueInfo, keyItem, out table);
            if (status == StatusError.None) TableList[name] = table;
            return status;
        }

        public StatusError CreateTable(string name, List<EntityInfo> valueInfo, int keyItem) {
            BaseTable table;
            return CreateTable(name, valueInfo, keyItem, out table);
        }

        public StatusError RenameTable(string name, string newName) {
            if (!TableList.ContainsKey(name)) return StatusError.TableNotFound;
            if (TableList.ContainsKey(newName)) return StatusError.TableAlreadyExist;
            BaseTable table = TableList[name];
            if (table != null) {
                TableList[name] = null;
                DataController.Close(table);
            }
            var status = DataController.Rename(name, newName);
            if (status == StatusError.None) {
                TableList.Remove(name);
                TableList[newName] = null;
            }
            return status;
        }

        public StatusError CloseTable(string name) {
            if (!TableList.ContainsKey(name)) return StatusError.TableNotFound;
            else {
                BaseTable table = TableList[name];
                if (table != null && DataController.Close(table)) {
                    TableList[name] = null;
                }
                return StatusError.None;
            }
        }

        public StatusError RemoveTable(string name) {
            if (TableList.ContainsKey(name)) {
                BaseTable table = TableList[name];
                if (table != null) DataController.Close(table);
                DataController.Remove(name);
                TableList.Remove(name);
                return StatusError.None;
            } else return StatusError.TableNotFound;
        }

        public StatusError GetTable(string name, out BaseTable table) {
            if (!TableList.TryGetValue(name, out table)) return StatusError.TableNotFound;
            if (table == null) {
                var status = DataController.Open(name, out table);
                if (status != StatusError.None) return status;
                TableList[name] = table;
            }
            return StatusError.None;
        }

        public ReadOnlyDictionary<string, BaseTable> GetTableList() {
            return new ReadOnlyDictionary<string, BaseTable>(TableList);
        }

        private StatusError ValidateValueInfo(List <EntityInfo> header) {
            HashSet<string> fildNames = new HashSet<string>();
            foreach (var inf in header) {
                if (inf.Name.Length == 0) return StatusError.FieldNameIsEmpty;
                if (!fildNames.Add(inf.Name)) return StatusError.FieldRedeclaration;
            }
            return StatusError.None;
        }

        public string GenerateTableName(string prefix) {
            int index = 0;
            string name;
            do {
                name = prefix + index.ToString();
                index++;
            } while (TableList.ContainsKey(name) || File.Exists(name));
            return name;
        }

        public StatusError ExecuteQuery(string code, out string error, string targetTableName = "") {
            var provider = CodeDomProvider.CreateProvider("CSharp");
            var par = new CompilerParameters();
            par.GenerateInMemory = true;
            par.GenerateExecutable = false;
            par.TreatWarningsAsErrors = false;
            par.ReferencedAssemblies.Add("System.dll");
            par.ReferencedAssemblies.Add("Core.dll");

            code = String.Format(@"
                using System;
                using System.Collections.Generic;
                using System.Collections.ObjectModel;
                using System.Text;
                using DBLab;

                public static class Query {{{0}}}
            ", code);

            var query = provider.CompileAssemblyFromSource(par, code);
            if (query.Errors.Count > 0) {
                var buffer = new StringBuilder();
                foreach (CompilerError err in query.Errors) {
                    buffer.Append("Line ");
                    buffer.Append(err.Line - 7);
                    buffer.Append(": ");
                    buffer.Append(err.ErrorText);
                    buffer.Append("\n");
                }
                error = buffer.ToString();
                return StatusError.QueryCompileError;
            }

            Type queryClassType = query.CompiledAssembly.GetType("Query");
            var runMethond = queryClassType.GetMethod("Run");

            BaseTable target = null;
            if (targetTableName.Length > 0) target = TableList[targetTableName];

            StatusError status;
            try {
                error = "";
                status = (StatusError)runMethond.Invoke(null, new object[] { this, target });
            } catch (Exception e) {
                error = e.ToString();
                status = StatusError.QueryRuntimeError;
            }

            return status;
        }
    }
}