public static StatusError Run(CoreModule core, CoreModule.BaseTable target) {

    CoreModule.BaseTable argTable;
    var status = core.GetTable("Sequence_Args", out argTable);
    if (status != StatusError.None) return status;

    List<EntityInfo> resultColumns = new List<EntityInfo>();
    resultColumns.Add(new EntityInfo("Time", EntityType.Int));
    resultColumns.Add(new EntityInfo("Type", EntityType.String));
    resultColumns.Add(new EntityInfo("Value", EntityType.Float));

    foreach (var argLine in argTable) {
        
        Console.WriteLine(argLine.Count);
        long begin = argLine[1].ToInt();
        long end = argLine[2].ToInt();



        CoreModule.BaseTable resultTable;
        status = core.CreateTable(core.GenerateTableName(string.Format("{0}_LocalMinMax_Result[{1}-{2}]_", target.GetName(), begin, end)), resultColumns, 0, out resultTable);
        if (status != StatusError.None) return status;

        double[] buffer = new double[2];
        int index = 0;
        long lastTime = 0;

        foreach (var line in target) {
            long time = line[0].ToInt();
            if (time < begin) continue;
            if (time > end) break;

            double value = line[1].ToFloat();
            if (index > 1) {
                if (buffer[0] < buffer[1] && buffer[1] > value) {
                    resultTable.InsertLine(lastTime, "MAX", buffer[1]);
                } else if (buffer[0] > buffer[1] && buffer[1] < value) {
                    resultTable.InsertLine(lastTime, "MIN", buffer[1]);
                }
            }
            buffer[0] = buffer[1];
            buffer[1] = value;
            index++;
            lastTime = time;
        }
    }

    return StatusError.None;
}