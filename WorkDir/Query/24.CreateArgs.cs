public static StatusError Run(CoreModule core, CoreModule.BaseTable target) {
    List<EntityInfo> columns = new List<EntityInfo>();
    columns.Add(new EntityInfo("ID", EntityType.Int));
    columns.Add(new EntityInfo("Begin", EntityType.Int));
    columns.Add(new EntityInfo("End", EntityType.Int));

    CoreModule.BaseTable table;
    return core.CreateTable("Sequence_Args", columns, 0, out table);
}
