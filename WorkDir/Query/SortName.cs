public static StatusError Run(CoreModule core, CoreModule.BaseTable target) {

    List<string> nameList = new List<string>(target.GetLineCount());
    int index = target.GetColumnIndex("Name");
    foreach (var line in target) {
        nameList.Add(line[index].ToString());
    }
    nameList.Sort();

    List<EntityInfo> columns = new List<EntityInfo>(2);
    columns.Add(new EntityInfo("#", EntityType.Int));
    columns.Add(new EntityInfo("Name", EntityType.String));

    CoreModule.BaseTable output;
    core.CreateTable(core.GenerateTableName("Query: SortName"), columns, 0, out output);

    for (int i = 0; i < nameList.Count; i++) {
        output.InsertLine(new List<EntityValue>() { i, nameList[i] });
    }

    return StatusError.None;
}
