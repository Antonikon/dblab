public static StatusError Run(CoreModule core, CoreModule.BaseTable target) {
    List<EntityInfo> columns = new List<EntityInfo>();
    columns.Add(new EntityInfo("Time", EntityType.Int));
    columns.Add(new EntityInfo("Value", EntityType.Float));

    CoreModule.BaseTable table;
    core.CreateTable(core.GenerateTableName("Sequence_"), columns, 0, out table);

    var rnd = new Random();
    int amount = rnd.Next(50, 150);
    int time = 0;
    for (int i = 0; i < amount; i++) {
        time += rnd.Next(0, 100);
        table.InsertLine(new List<EntityValue>() { time, rnd.NextDouble() * 5 });
    }
    return StatusError.None;
}
