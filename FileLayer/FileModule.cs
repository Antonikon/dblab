﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace DBLab {
    //реалищация таблицы на файлах, наследуется от универсального интерфейса
    public class FileTable : CoreModule.BaseTable {
        //Файловый модуль, управляет созданием и загрузкой таблиц
        public class FileModule : DataModule {
            private string TablePath;

            public FileModule(string path) {
                TablePath = path;
                Directory.CreateDirectory(path);
            }

            public StatusError Create(string name, List<EntityInfo> valueInfo, int keyItem, out CoreModule.BaseTable table) {
                table = null;

                if (keyItem < 0 || keyItem >= valueInfo.Count) return StatusError.PrimaryKeyNotFound;

                string path = TablePath + "/" + name;
                if (File.Exists(path)) return StatusError.TableAlreadyExist;

                FileStream stream = null;
                try {
                    stream = File.Open(path, FileMode.Create);
                } catch (Exception) {
                    return StatusError.InvalidTableName;
                }

                FileTable fileTable = new FileTable();
                fileTable.Create(stream, name, valueInfo, keyItem);
                table = fileTable;
                return StatusError.None;
            }


            public StatusError Open(string name, out CoreModule.BaseTable table) {
                table = null;
                string path = TablePath + "/" + name;
                if (!File.Exists(path)) return StatusError.TableNotFound;

                FileStream stream = null;
                try {
                    stream = File.Open(path, FileMode.Open);
                } catch (Exception) {
                    return StatusError.InvalidTableName;
                };

                FileTable fileTable = new FileTable();
                var status = fileTable.Open(stream, name);
                if (status == StatusError.None) table = fileTable;
                else fileTable.Close();

                return status;               
            }

            public bool Remove(string name) {
                File.Delete(TablePath + "/" + name);
                return true;
            }

            public string[] GetTableList() {
                var files = Directory.GetFiles(TablePath);
                string[] names = new string[files.Length];
                for (int i = 0; i < files.Length; i++) {
                    names[i] = Path.GetFileName(files[i]);
                }
                return names;
            }

            public bool Close(CoreModule.BaseTable table) {
                FileTable fileTable = (FileTable)table;
                fileTable.Flush();
                fileTable.Close();
                return true;
            }

            public StatusError Rename(string name, string newName) {
                string path = TablePath + "/" + name;
                string newPath = TablePath + "/" + newName;
                if (!File.Exists(path)) return StatusError.TableNotFound;
                if (File.Exists(newName)) return StatusError.TableAlreadyExist;
                try {
                    File.Move(path, newPath);
                } catch (Exception) {
                    return StatusError.InvalidTableName;
                }
                return StatusError.None;
            }
        }

        private FileStream TableFile;
        private StreamReader TableReader;
        private StreamWriter TableWriter;

        private SortedDictionary<EntityValue, List<EntityValue>> Data = new SortedDictionary<EntityValue, List<EntityValue>>();
        private HashSet<EntityValue> ModifiedLines = new HashSet<EntityValue>();
        static public uint BufferSize = 16;

        private FileTable() { }

        public StatusError Open(FileStream stream, string name) {
            Name = name;
            TableFile = stream;
            TableReader = new StreamReader(TableFile);
            TableWriter = new StreamWriter(TableFile);
            return Parse();
        }

        public void Create(FileStream stream, string name, List<EntityInfo> valueInfo, int keyIndex) {
            Name = name;
            TableFile = stream;
            TableReader = new StreamReader(TableFile);
            TableWriter = new StreamWriter(TableFile);
            ValueInfo = new List<EntityInfo>(valueInfo);
            KeyIndex = keyIndex;
        }

        private StatusError ParseHeader() {
            KeyIndex = -1;
            if (TableReader.EndOfStream) return StatusError.HeaderParseError;
            string[] header = TableReader.ReadLine().Split(';');
            for (int i = 0; i < header.Length - 1; i++) {
                string[] itm = header[i].Split(':');
                if (itm.Length != 2) return StatusError.HeaderParseError;

                if (itm[0][0] == '!') {
                    KeyIndex = i;
                    itm[0] = itm[0].Substring(1);
                }

                if (itm[0].Length == 0) return StatusError.HeaderParseError;

                EntityType type = itm[1].ToEntityType();
                if (type == EntityType.None) return StatusError.HeaderParseError;

                ValueInfo.Add(new EntityInfo(itm[0], type));
            }
            if (KeyIndex == -1) return StatusError.PrimaryKeyNotFound;
            return StatusError.None;
        }

        private StatusError ParseLine() {
            List<EntityValue> line = new List<EntityValue>();

            string data = TableReader.ReadLine();
            StringBuilder buffer = new StringBuilder();
            bool quotation = false;
            bool forceFlush = false;

            for (int i = 0; i < data.Length; i++) {
                switch (data[i]) {
                    case '\\':
                        i++;
                        break;
                    case '\"':
                        quotation = !quotation;
                        forceFlush = true;
                        continue;
                    case ' ':
                        if (!quotation && (forceFlush || buffer.Length > 0)) {
                            EntityValue value;
                            if (!EntityValue.SafeCreate(ValueInfo[line.Count].Type, buffer.ToString(), out value)) return StatusError.LineParseError;
                            line.Add(value);
                            buffer.Clear();
                            forceFlush = false;
                            continue;
                        }
                        break;
                }
                buffer.Append(data[i]);
            }
            if (quotation || buffer.Length > 0 || line.Count != ValueInfo.Count) return StatusError.LineParseError;

            Data[line[(int)KeyIndex]] = line;
            return StatusError.None;
        }

        private StatusError Parse() {
            TableReader.BaseStream.Seek(0, SeekOrigin.Begin);
            var status = ParseHeader();
            if (status != StatusError.None) return status;
            while (!TableReader.EndOfStream) {
                status = ParseLine();
                if (status != StatusError.None) return status;
            }
            return StatusError.None;
        }

        private void WriteHeader() {
            for (int i = 0; i < ValueInfo.Count; i++) {
                EntityInfo type = ValueInfo[i];
                if (i == KeyIndex) TableWriter.Write("!");
                TableWriter.Write(type.Name);
                TableWriter.Write(":");
                TableWriter.Write(type.GetTypeName());
                TableWriter.Write(";");
            }
            TableWriter.Write("\n");
        }

        private void WriteContent() {
            foreach (var line in Data.Values) {
                foreach (EntityValue value in line) {
                    if (value.Type == EntityType.String) {
                        TableWriter.Write("\"");
                        var formattedLine = value.ToString().Replace("\\", "\\\\").Replace("\"", "\\\"");
                        TableWriter.Write(formattedLine);
                        TableWriter.Write("\"");
                    } else {
                        TableWriter.Write(value);
                    }
                    TableWriter.Write(" ");
                }
                TableWriter.Write("\n");
            }
        }

        private void FlushUpdate() {
            if (ModifiedLines.Count > BufferSize) Flush();
        }

        private void Flush() {
            ModifiedLines.Clear();
            TableWriter.BaseStream.Seek(0, SeekOrigin.Begin);
            TableFile.SetLength(0);
            TableFile.Flush();
            WriteHeader();
            WriteContent();
            TableWriter.Flush();
        }

        public override IList<EntityValue> GetLine(EntityValue key) {
            return Data[key].AsReadOnly();
        }

        public override EntityValue GetValue(EntityValue key, string name) {
            List<EntityValue> line = Data[key];
            int index = GetColumnIndex(name);
            //TODO: Add StatusError
            return line[index];
        }

        public override EntityValue GetValue(EntityValue key, int index) {
            return Data[key][(int)index];
        }

        public override StatusError SetValue(EntityValue key, string name, EntityValue value) {
            if (GetKeyInfo().Name == name) return StatusError.KeyCannotBeChanged;
            List<EntityValue> line = Data[key];
            int index = GetColumnIndex(name);
            if (index == -1) return StatusError.FieldDoesntExist;
            line[index] = value;
            ModifiedLines.Add(key);
            FlushUpdate();
            return StatusError.None;
        }

        public override StatusError SetValue(EntityValue key, int index, EntityValue value) {
            if (KeyIndex == index) return StatusError.KeyCannotBeChanged;
            Data[key][(int)index] = value;
            return StatusError.None;
        }

        public override StatusError InsertLine(List<EntityValue> line) {
            EntityValue key = line[(int)KeyIndex];
            if (Data.ContainsKey(key)) return StatusError.KeyAlreadyExist;

            Data[key] = new List<EntityValue>(line);
            ModifiedLines.Add(key);
            FlushUpdate();
            return StatusError.None;
        }

        public override StatusError UpdateLine(List<EntityValue> line) {
            EntityValue key = line[(int)KeyIndex];
            if (!Data.ContainsKey(key)) return StatusError.KeyNotFound;

            Data[key] = new List<EntityValue>(line);
            ModifiedLines.Add(key);
            FlushUpdate();
            return StatusError.None;
        }

        public override StatusError UpdateKey(EntityValue key, EntityValue newKey) {
            if (!Data.ContainsKey(key)) return StatusError.KeyNotFound;
            if (Data.ContainsKey(newKey)) return StatusError.KeyAlreadyExist;

            var line = Data[key];
            line[KeyIndex] = newKey;
            Data[newKey] = line;
            Data.Remove(key);
            ModifiedLines.Add(key);
            ModifiedLines.Add(newKey);
            FlushUpdate();
            return StatusError.None;
        }

        public override StatusError RemoveLine(EntityValue key) {
            if (!Data.Remove(key)) return StatusError.KeyNotFound;
            ModifiedLines.Add(key);
            FlushUpdate();
            return StatusError.None;
        }

        protected void Close() {
            TableWriter.Close();
            TableReader.Close();
            TableFile.Close();
        }

        public override bool Contains(EntityValue key) {
            return Data.ContainsKey(key);
        }

        public override IEnumerator<List<EntityValue>> GetEnumerator() {
            foreach (var line in Data.Values) {
                yield return line;
            }
        }

        public override int GetLineCount() {
            return Data.Count;
        }
    }
}